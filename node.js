var app = require('express')();
var bodyParser = require('body-parser');
var multer = require('multer');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(multer()); // for parsing multipart/form-data
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:9000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



var SabreDevStudio = require('sabre-dev-studio');
var sabre_dev_studio = new SabreDevStudio({
    client_id:     'V1:ueuxeewdr0hqocls:DEVCENTER:EXT',
    client_secret: 'Lxjt2M2U',
    uri:           'https://api.test.sabre.com'
});
var options = {};


app.get('/*', function (req, res) {
    sabre_dev_studio.get(req.url, options, function(error, data) {
        if (error) {
            res.type('application/json').status(error.status).send(error);
            res.end();
        } else {
            console.log(JSON.stringify(JSON.parse(data)));
            res.type('application/json').status(200).send(data);
            res.end();
        }
    });
    //console.log(req.url);
});
app.post('/*', function (req, res) {
    sabre_dev_studio.post(req.url ,JSON.stringify(req.body) , options, function(error, data) {
        if (error) {
            //res.type('application/json').status(error.status).json(error);
            res.end();
        } else {
            //console.log(JSON.stringify(JSON.parse(data)));
            res.type('application/json').status(200).send(data);
            res.end();
        }
    });
    //console.log('respbegin');
    //console.log(req.body);
    //res.status(200).send('');
    //res.end();
});

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});



//sabre_dev_studio.request('POST','/v1.8.6/shop/flights?mode=live', obj1, options, callback);
//sabre_dev_studio.get('/v1/lists/supported/shop/themes', options, callback);
//obj = {
//    "OTA_AirLowFareSearchRQ":{
//        "OriginDestinationInformation":[
//            {
//                "DepartureDateTime":"2015-07-16T00:00:00",
//                "DestinationLocation":{
//                    "LocationCode":"LAS"
//                },
//                "OriginLocation":{
//                    "LocationCode":"DFW"
//                },
//                "RPH":"1",
//                "TPA_Extensions":{
//                    "SegmentType":{
//                        "Code":"O"
//                    }
//                }
//            },
//            {
//                "DepartureDateTime":"2015-07-23T00:00:00",
//                "DestinationLocation":{
//                    "LocationCode":"DFW"
//                },
//                "OriginLocation":{
//                    "LocationCode":"LAS"
//                },
//                "RPH":"2",
//                "TPA_Extensions":{
//                    "SegmentType":{
//                        "Code":"O"
//                    }
//                }
//            }
//        ],
//        "POS":{
//            "Source":[
//                {
//                    "RequestorID":{
//                        "CompanyName":{
//                            "Code":"TN",
//                            "content":"TN"
//                        },
//                        "ID":"1",
//                        "Type":"1"
//                    }
//                }
//            ]
//        },
//        "TPA_Extensions":{
//            "IntelliSellTransaction":{
//                "RequestType":{
//                    "Name":"50ITINS"
//                }
//            }
//        },
//        "Target":"Production",
//        "TravelPreferences":{
//            "CabinPref":[
//                {
//                    "Cabin":"Y"
//                }
//            ],
//            "TPA_Extensions":{
//                "TripType":{
//                }
//            }
//        },
//        "TravelerInfoSummary":{
//            "AirTravelerAvail":[
//                {
//                    "PassengerTypeQuantity":[
//                        {
//                            "Code":"ADT",
//                            "Quantity":1
//                        }
//                    ]
//                }
//            ],
//            "PriceRequestInformation":{
//                "CurrencyCode":"USD"
//            },
//            "SeatsRequested":[
//                1
//            ]
//        }
//    }
//};
//obj1 = JSON.stringify(obj);
